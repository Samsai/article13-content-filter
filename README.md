## What is it?

This project is a simple content filtering algorithm, written in Python but easily adaptable to other programming
languages and environments, meant to filter user-provided content in a way that effectively combats copyright
violations while minimizing false-flags where user-owned content is falsely identified as a copyrighted work.

## But why?

EU has voted in favour of the Copyright Directive Articles 11 and 13. Article 13 specifically requires
online platforms to combat copyright violations by implementing automated filtering systems, such as
the ContentID system.

However, ContentID is notorious for its false-flags, where users have had their legitimate content be claimed
by various "copyright holders".

This project aims to implement a function that platforms can easily adapt that filters out copyrighted content and 
minimizes the amount of false-flags by utilizing a never-before-seen algorithm that should yield significantly
better results.

## Contributions and patches

This algorithm is perfect. It does not need contributions and pull requests will likely be ignored. However,
you are allowed to use, mis-use, abuse and adapt the provided algorithm as stated in the LICENSE.

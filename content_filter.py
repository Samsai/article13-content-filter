import random

def content_filter(content_hash):
    """ Take a file's hash and decide whether the file should be filtered or not.
        Returns True if the file is allowed, False otherwise."""

    if random.random() >= 0.5:
        return False
    else:
        return True
